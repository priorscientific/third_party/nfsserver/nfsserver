//
// NFS Server
//
// Copyright (c) 2004-2012 Pete Barber
//
// Licensed under the The Code Project Open License (CPOL.html)
// http://www.codeproject.com/info/cpol10.aspx 
//
using System;
using System.IO;
using RPCV2Lib;

namespace nfsV2
{
	/// <summary>
	/// Summary description for readdirres.
	/// </summary>
	public class entry
	{
		private uint	fileid;
		private string	name;
		private uint	cookie;

		// Special constructor for "." and ".."
		public entry(string name, string fullName, uint cookie, uint fileid)
		{
			this.fileid = fileid;
			this.name	= name;
			this.cookie	= cookie;

			//Console.WriteLine("entry fileid:{0,5}, cookie:{1,5}, name:{2}", fileid, cookie, name);
		}

		public entry(FileSystemInfo info, uint cookie)
		{
			this.name = info.Name;

			if (info.Attributes != FileAttributes.Directory && 
				info.Attributes != FileAttributes.Device	&& 
				info.Extension == ".sl")
				this.name = this.name.Remove(this.name.Length - 3, 3);

			this.cookie	= cookie;

			fhandle fh;

			if ((fh = FileTable.LookupFileHandle(info.FullName)) == null)
				fh = FileTable.Add(new FileEntry(info.FullName));

			this.fileid = fh.Index;

			//Console.WriteLine("entry fileid:{0,5}, cookie:{1,5}, name:{2}", fileid, cookie, name);
		}

		public void Pack(rpcPacker packer)
		{
			//Console.WriteLine("entry pack name:{0}", name);

			packer.setUint32(fileid);
			packer.setString(name);
			packer.setUint32(cookie);
		}

		public uint Size
		{
			get
			{
				return 12 + rpcPacker.sizeOfString(name);
			}
		}
	}

	public class readdirres
	{
		entry[] entries;

        public readdirres(string dirName, uint count)
		{
            string dirLocalName = FileTable.ExternalToLocal(dirName);

#if DEBUG
            Console.WriteLine("readdirres: {0}:{1}", dirName, dirLocalName);
#endif

            DirectoryInfo dir = new DirectoryInfo(dirLocalName);

			FileSystemInfo[] files = dir.GetFileSystemInfos();

			entries = new entry[files.Length + 2];

            // Sort out handle for this directory
            fhandle fh = FileTable.LookupFileHandle(dirName);
            if (fh == null)
            {
                fh = FileTable.Add(new FileEntry(dirName));
            }

			uint dirFileId = fh.Index;

			entries[0] = new entry(".", dirName + @"\.", 1, dirFileId);

            uint cookie;
            if (files.Length == 0)
            {
                cookie = count;
            }
            else
            {
                cookie = 2;
            }

            if (dirFileId == 1) // root
            {
                entries[1] = new entry("..", dirName + @"\..", cookie, 1);
            }
            else
            {
                // Sort out handle for directory above
                String parentName = FileTable.LocalToExternal(dir.Parent.FullName);
                fh = FileTable.LookupFileHandle(parentName);
                if (fh == null)
                {
                    fh = FileTable.Add(new FileEntry(parentName));
                }

                entries[1] = new entry("..", dirName + @"\..", cookie, fh.Index);
            }

			uint i = 2;

			foreach (FileSystemInfo file in files)
				if (files.Length == i - 1)
					entries[i] = new entry(file, count);
				else
					entries[i] = new entry(file, ++i);
		}

		public bool Pack(rpcPacker packer, uint cookie, uint count)
		{

			packer.setUint32((uint)NFSStatus.NFS_OK);

      		uint size = 8;	// First pointer + EOF

			if (cookie >= entries.Length)
			{
				// nothing
			}
			else
			{
				do
				{
					entry next = entries[cookie];

					if (size + next.Size > count)
						break;
					else
						size += next.Size;
	
					// true as in yes, more follows.  This is *entry.
					packer.setUint32(1);

					next.Pack(packer);
				}
				while (++cookie < entries.Length);
			}

			// false as in no more follow.  This is *entry.
			// Unlike EOF which is set only when all entries have been sent
			// *entry is reset to false following the last entry in each
			// batch.
			packer.setUint32(0);

			//Console.WriteLine("ReadDir: Pack done.  cookie:{0}, size:{1}", cookie, size);

			// EOF
			if (cookie >= entries.Length)
			{
				packer.setUint32((uint)1);	// yes
				return true;
			}
			else
			{
				packer.setUint32((uint)0);	// no
				return false;
			}
		}
	}
}
