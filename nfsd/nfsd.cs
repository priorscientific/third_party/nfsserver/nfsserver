//
// NFS Server
//
// Copyright (c) 2004-2012 Pete Barber
//
// Licensed under the The Code Project Open License (CPOL.html)
// http://www.codeproject.com/info/cpol10.aspx 
//
using System;
using System.IO;
using System.Text;

using RPCV2Lib;

namespace nfsV2
{
	/// <summary>
	/// Summary description for nfsd.
	/// </summary>
	/// 
	public class nfsd : rpcd
	{
		private readdirres	results;
		private int			MAXPATHLEN =  1024;

		public nfsd() : base(Ports.nfsd, Progs.nfsd)
		{
		}

		protected override bool Prog(uint prog)
		{
			return prog == 100227 ? true : false;
		}

		protected override void Proc(uint proc, rpcCracker cracker, rpcPacker packer)
		{
			try
			{
				switch(proc)
				{
					case 1:
						GetAttr(cracker, packer);
						break;
					case 2:
						SetAttr(cracker, packer);
						break;
					case 3:
						// Root(). No-op.
						break;
					case 4:
						Lookup(cracker, packer);
						break;
					case 5:
						ReadLink(cracker, packer);
						break;
					case 6:
						Read(cracker, packer);
						break;
					case 8:
						Write(cracker, packer);
						break;
					case 9:
						Create(cracker, packer);
						break;
					case 10:
						Remove(cracker, packer);
						break;
					case 11:
						Rename(cracker, packer);
						break;
					case 13:
						SymLink(cracker, packer);
						break;
					case 14:
						MkDir(cracker, packer);
						break;
					case 15:
						RmDir(cracker, packer);
						break;
					case 16:
						ReadDir(cracker, packer);
						break;
					case 17:
						StatFS(cracker, packer);
						break;
					default:
						throw new BadProc();
				}
			}
			catch(BadProc)
			{
				throw;
			}
			catch(NFSStatusException e)
			{
#if DEBUG
                Console.WriteLine("NFSStatusException:{0}", (uint)e.Status);
#endif
				packer.setUint32((uint)e.Status);
			}
			catch (System.IO.FileNotFoundException)
            {
#if DEBUG
                Console.WriteLine("FileNotFoundException");
#endif
				packer.setUint32((uint)NFSStatus.NFSERR_NOENT);
			}
			catch(UnauthorizedAccessException)
			{
#if DEBUG
                Console.WriteLine("UnauthorizedAccessException");
#endif
                packer.setUint32((uint)NFSStatus.NFSERR_PERM);
			}
			catch(PathTooLongException)
			{
#if DEBUG
                Console.WriteLine("PathTooLongException");
#endif
                packer.setUint32((uint)NFSStatus.NFSERR_NAMETOOLONG);
			}
			catch(DirectoryNotFoundException)
			{
#if DEBUG
                Console.WriteLine("DirectoryNotFoundException");
#endif
                packer.setUint32((uint)NFSStatus.NFSERR_NOTDIR);
			}
			catch(Exception e)
			{
				Console.WriteLine("nfsd error:{0}", e);
				packer.setUint32((uint)NFSStatus.NFSERR_IO);
			}
		}

		private void GetAttr(rpcCracker cracker, rpcPacker packer)
		{
			attrstat.PackSuccess(packer, new fattr(new fhandle(cracker)));
		}

		private void SetAttr(rpcCracker cracker, rpcPacker packer)
		{
			fhandle	fh			= new fhandle(cracker);
			sattr	attributes	= new sattr(cracker);

			FileEntry file = FileTable.LookupFileEntry(fh);

			if (file == null)
			{
#if DEBUG
                Console.WriteLine("Invalid file handle:{0}", fh.Index);
#endif
				throw new NFSStatusException(NFSStatus.NFSERR_STALE);
			}

            String localName = FileTable.ExternalToLocal(file.Name);

			// TODO: Actually do something with the attributes.
			if (attributes.Size == 0)
			{
				try
				{
                    FileStream fs = new FileStream(localName, FileMode.Truncate, FileAccess.Write);
					fs.Close();
				}
				catch (System.IO.FileNotFoundException)
				{
					FileTable.Remove(fh);
					throw;
				}
			}

			if ((int)attributes.Mode != -1)
			{
                FileInfo info = new FileInfo(localName);

				if ((attributes.Mode & (uint)fattr.modes.WOWN) == (uint)fattr.modes.WOWN)
					info.Attributes = info.Attributes & ~FileAttributes.ReadOnly;
				else
					info.Attributes = info.Attributes | FileAttributes.ReadOnly;
			}

			attrstat.PackSuccess(packer, new fattr(fh));
		}

		private void Lookup(rpcCracker cracker, rpcPacker packer)
		{
			diropargs args = new diropargs(cracker);

			String lookupPath			= FileTable.LookupFileEntry(args.DirHandle).Name + @"/" + args.FileName;
			String symLinkLookupPath	= lookupPath + ".sl";

#if DEBUG
			//Console.WriteLine(@"Lookup: {0}", lookupPath);
#endif

			fhandle fh = null;

            try
            {
                if ((fh = FileTable.LookupFileHandle(lookupPath)) == null)
                {
                    //Console.WriteLine(@"Lookup (symlink): {0}", symLinkLookupPath);

                    fh = FileTable.LookupFileHandle(symLinkLookupPath);
                }

                // Entry (for file or symlink) not in FileTable
                if (fh == null)
                {
                    // Try non-SL first
                    fh = FileTable.Add(new FileEntry(lookupPath));

                    try
                    {
                        diropres.PackSuccess(packer, fh, new fattr(fh));
                    }
                    catch
                    {
                        FileTable.Remove(fh);

                        fh = FileTable.Add(new FileEntry(symLinkLookupPath));
                        try
                        {
                            diropres.PackSuccess(packer, fh, new fattr(fh));
                        }
                        catch
                        {
                            FileTable.Remove(fh);
                            throw;
                        }
                    }
                }
                else
                {
                    diropres.PackSuccess(packer, fh, new fattr(fh));
                }
            }
            catch (System.ArgumentException e)
            {
                // Caused by filename with invalid chars, so file not found is good enough
                throw new System.IO.FileNotFoundException();
            }
            catch (System.Exception e)
            {
#if DEBUG
                Console.WriteLine(@"Lookup EXCEPTION: {0}", lookupPath);
#endif
                throw;
            }
		}

		private void ReadLink(rpcCracker cracker, rpcPacker packer)
		{
			fhandle fh = new fhandle(cracker);

			FileStream fs;
			
			try
			{
				fs = new FileStream(FileTable.ExternalToLocal(FileTable.LookupFileEntry(fh).Name), FileMode.Open, FileAccess.Read);
			}
			catch (System.IO.FileNotFoundException)
			{
				FileTable.Remove(fh);
				throw;
			}

			try
			{
				Byte[] buf = new Byte[MAXPATHLEN];

				int bytesRead = fs.Read(buf, 0, MAXPATHLEN);

				packer.setUint32((uint)NFSStatus.NFS_OK);
				packer.setData(buf, buf.Length);
			}
			finally
			{
				fs.Close();
			}

		}

		private void Read(rpcCracker cracker, rpcPacker packer)
		{
			fhandle fh			= new fhandle(cracker);
			uint	offset		= cracker.get_uint32();
			uint	count		= cracker.get_uint32();
			uint	totalCount	= cracker.get_uint32();

#if DEBUG
            Console.WriteLine("Offset, count, totalCount: {0}, {1}, {2}", offset, count, totalCount);
#endif

			FileStream fs;
            String localName = FileTable.ExternalToLocal(FileTable.LookupFileEntry(fh).Name);
			
			try
			{
                fs = new FileStream(localName, FileMode.Open, FileAccess.Read);
			}
			catch (System.IO.FileNotFoundException)
			{
#if DEBUG
                Console.WriteLine("Read failed: {0}", localName);
#endif
				FileTable.Remove(fh);
				throw;
			}

			try
			{
				fs.Position = offset;

				Byte[] buf = new Byte[count];

				int bytesRead = fs.Read(buf, 0, (int)count);

				fattr attr = new fattr(fh);

				if (attr.IsFile() == false) throw new NFSStatusException(NFSStatus.NFSERR_ISDIR);

				packer.setUint32((uint)NFSStatus.NFS_OK);
				attr.Pack(packer);
				packer.setData(buf, bytesRead);
#if DEBUG
                Console.WriteLine("Read file: {0}: {1}", localName, bytesRead);
#endif
			}
			finally
			{
				fs.Close();
			}
		}

		private void Write(rpcCracker cracker, rpcPacker packer)
		{
			fhandle	fh			= new fhandle(cracker);
			uint	beginOffset	= cracker.get_uint32();
			uint	offset		= cracker.get_uint32();
			uint	totalcount	= cracker.get_uint32();
			Byte[] data			= cracker.getData();

			FileStream fs;
            String localName = FileTable.ExternalToLocal(FileTable.LookupFileEntry(fh).Name);
			
			try
			{
                fs = new FileStream(localName, FileMode.Open, FileAccess.Write);
			}
			catch (System.IO.FileNotFoundException)
			{
				FileTable.Remove(fh);
				throw;
			}

			try
			{
				fs.Position = offset;

				fs.Write(data, 0, data.Length);
			
				attrstat.PackSuccess(packer, new fattr(fh));
			}
			finally
			{
				fs.Close();
			}
		}

		private void Create(rpcCracker cracker, rpcPacker packer)
		{
			CreateFileOrDirectory(cracker, packer, true);
		}

		private void Remove(rpcCracker cracker, rpcPacker packer)
		{
			diropargs args = new diropargs(cracker);

			String removePath = FileTable.LookupFileEntry(args.DirHandle).Name + "/" + args.FileName;
            String removePathLocal = FileTable.ExternalToLocal(removePath);

            FileInfo info = new FileInfo(removePathLocal);

			if (info.Exists == false)
			{
                removePathLocal += ".sl";
                info = new FileInfo(removePathLocal);
			}

#if DEBUG
            Console.WriteLine(@"Remove: {0}:{1}", removePath, removePathLocal);
#endif

			fhandle fh = FileTable.LookupFileHandle(removePath);

			info.Delete();
			// If UnauthorizedAccessException is thrown & caught should 
			// probably stat file to determine if the cause is because
			// the path is a dir rather than a directory.

			if (fh != null) FileTable.Remove(fh);

			packer.setUint32((uint)NFSStatus.NFS_OK);
		}

		private void Rename(rpcCracker cracker, rpcPacker packer)
		{
			diropargs from	= new diropargs(cracker);
			diropargs to	= new diropargs(cracker);

			string fromPath = FileTable.LookupFileEntry(from.DirHandle).Name + "/" + from.FileName;
            string toPath = FileTable.LookupFileEntry(to.DirHandle).Name + "/" + to.FileName;
            string fromPathLocal = FileTable.ExternalToLocal(fromPath);
            string toPathLocal = FileTable.ExternalToLocal(toPath); 

#if DEBUG
            Console.WriteLine("Rename {0} to {1}", fromPath, toPath);
#endif

            if (File.Exists(toPathLocal) == true)
                File.Delete(toPathLocal);

            File.Move(fromPathLocal, toPathLocal);

			// Only bother updating the FileTable if the operation was successful
			FileTable.Rename(fromPath, toPath);

			packer.setUint32((uint)NFSStatus.NFS_OK);
		}

		private void SymLink(rpcCracker cracker, rpcPacker packer)
		{
			diropargs	args	= new diropargs(cracker);
			string		path	= cracker.get_String();
			sattr		attr	= new sattr(cracker);

			String createPath = FileTable.LookupFileEntry(args.DirHandle).Name + "/" + args.FileName + ".sl";
            String createPathLocal = FileTable.ExternalToLocal(createPath);

#if DEBUG
            Console.WriteLine("Symlink: {0}->{1}", createPath, path);
#endif

			fhandle fh;

			if ((fh = FileTable.LookupFileHandle(createPath)) == null)
				fh = FileTable.Add(new FileEntry(createPath));

			try
			{
                FileStream symlink = new FileStream(createPathLocal, FileMode.CreateNew, FileAccess.Write);

				try
				{
					UTF8Encoding pathUTF8 = new UTF8Encoding();

					byte[] buf = pathUTF8.GetBytes(path);
	
					symlink.Write(buf, 0, buf.Length);

					packer.setUint32((uint)NFSStatus.NFS_OK);
				}
				finally
				{
					symlink.Close();
				}
			}
			catch(IOException)
			{
                if (new FileInfo(createPathLocal).Exists == true)
					throw new NFSStatusException(NFSStatus.NFSERR_EXIST);
				else
					throw;
			}
		}

		private void MkDir(rpcCracker cracker, rpcPacker packer)
		{
			CreateFileOrDirectory(cracker, packer, false);
		}

		private void RmDir(rpcCracker cracker, rpcPacker packer)
		{
			diropargs args = new diropargs(cracker);

			String removePath = FileTable.LookupFileEntry(args.DirHandle).Name + "/" + args.FileName;
            String removePathLocal = FileTable.ExternalToLocal(removePath);

#if DEBUG
            Console.WriteLine(@"RmDir: {0}", removePath);
#endif

			fhandle fh = FileTable.LookupFileHandle(removePath);

			try
			{
				new DirectoryInfo(removePath).Delete(false);
			}
			catch (IOException)
			{
				if (new DirectoryInfo(removePath).GetFileSystemInfos().Length > 0)
					throw new NFSStatusException(NFSStatus.NFSERR_NOTEMPTY);
				else
						throw new NFSStatusException(NFSStatus.NFSERR_PERM);
			}

			if (fh != null) FileTable.Remove(fh);

			packer.setUint32((uint)NFSStatus.NFS_OK);
		}

		private void ReadDir(rpcCracker cracker, rpcPacker packer)
		{
			fhandle fh	= new fhandle(cracker);
			uint cookie	= cracker.get_uint32();
			uint count	= cracker.get_uint32();

			FileEntry dir = FileTable.LookupFileEntry(fh);

#if DEBUG
            Console.WriteLine("ReadDir: cookie:{0}, count:{1}, resultsNULL:{2}", cookie, count, results == null);
#endif

			if (cookie == 0 || results == null)
			{
				if (dir == null) throw new NFSStatusException(NFSStatus.NFSERR_EXIST);

				try
				{
                    results = new readdirres(dir.Name, count);
				}
				catch(DirectoryNotFoundException)
				{
					FileTable.Remove(fh);
					throw;
				}
			}

			if (results.Pack(packer, cookie, count) == true)
				results = null;
		}

		private void StatFS(rpcCracker cracker, rpcPacker packer)
		{
			const uint BLOCK_SIZE = 4096;

			fhandle fh = new fhandle(cracker);

            String localName = FileTable.ExternalToLocal(FileTable.LookupFileEntry(fh).Name);

#if DEBUG
            Console.WriteLine("StatFS: {0}", localName);
#endif

			System.UInt64 freeBytesAvailable		= 0;
			System.UInt64 totalNumberOfBytes		= 0;
			System.UInt64 totalNumberOfFreeBytes	= 0;

            if (UnmanagedWin32API.GetDiskFreeSpaceEx(localName, ref freeBytesAvailable, ref totalNumberOfBytes, ref totalNumberOfFreeBytes) == false)
				throw new NFSStatusException(NFSStatus.NFSERR_EXIST);

			freeBytesAvailable		/= BLOCK_SIZE;
			totalNumberOfBytes		/= BLOCK_SIZE;
			totalNumberOfFreeBytes	/= BLOCK_SIZE;

			packer.setUint32((uint)NFSStatus.NFS_OK);
			packer.setUint32(BLOCK_SIZE);				// tsize: optimum transfer size
			packer.setUint32(BLOCK_SIZE);				// Block size of FS
			packer.setUint32((uint)totalNumberOfBytes);		// Total # of blocks (of the above size)
			packer.setUint32((uint)totalNumberOfFreeBytes);	// Free blocks
			packer.setUint32((uint)freeBytesAvailable);		// Free blocks available to non-priv. users
		}

		private void CreateFileOrDirectory(rpcCracker cracker, rpcPacker packer, bool createFile)
		{
			createargs args = new createargs(cracker);

			String createPath = FileTable.LookupFileEntry(args.Where.DirHandle).Name + "/" + args.Where.FileName;
            String createPathLocal = FileTable.ExternalToLocal(createPath);

#if DEBUG
            Console.WriteLine("Create: {0}", createPath);
#endif

			fhandle fh;

			if ((fh = FileTable.LookupFileHandle(createPath)) == null)
				fh = FileTable.Add(new FileEntry(createPath));

			if (createFile == true)
                new FileInfo(createPathLocal).Create().Close();
			else
                new DirectoryInfo(createPathLocal).Create();

			fattr attr = new fattr(fh);

			if (attr.IsFile() != createFile)
				throw new System.Exception();

			diropres.PackSuccess(packer, fh, attr);
		}
	}
}
