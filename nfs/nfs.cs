//
// NFS Server
//
// Copyright (c) 2004-2012 Pete Barber
//
// Licensed under the The Code Project Open License (CPOL.html)
// http://www.codeproject.com/info/cpol10.aspx 
//
using System;
using System.IO;
using System.Threading;

using RPCV2Lib;
using portmapperV1;
using mountV1;
using nfsV2;

namespace NFS
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class nfs
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            try
            {
                if (args.Length > 0)
                {
                    DirectoryInfo dir = new DirectoryInfo(args[0]);

                    if (!dir.Exists)
                    {
#if DEBUG
                        Console.WriteLine("Error: NFS root directory {0} does not exist", args[0]);
#endif
                        return 3;
                    }

                    FileTable.Root = args[0];
                }
                else
                {
#if DEBUG
                    Console.WriteLine("Command line: NFS <root>");
                    Console.WriteLine("Error: NFS root directory not specified");
#endif
                }

                // Set up file handle table singleton
                FileTable fileHandles = new FileTable(1024);

                // Set up worker threads to run NFS server
                portmapper portMapperInstance = new portmapper();
                mountd mountdInstance = new mountd();
                nfsd nfsdInstance = new nfsd();

                Thread portMapperThread = new Thread(new ThreadStart(portMapperInstance.Run));
                Thread mountDThread = new Thread(new ThreadStart(mountdInstance.Run));
                Thread nfsDThread = new Thread(new ThreadStart(nfsdInstance.Run));

                portMapperThread.Start();
                mountDThread.Start();
                nfsDThread.Start();

#if DEBUG
                Console.WriteLine("Type \"End\" and hit return to stop server");
#endif
                try
                {
                    // Run until we get the text "End"
                    string line;
                    bool loop = true;
                    while (loop)
                    {
                        line = Console.ReadLine();

                        if (line.StartsWith("End"))
                        {
                            loop = false;
                        }
                    }
                }
                catch
                {
                    // Exceptions in reading a keypress will also terminate the server
                }

                // Tear down worker threads in reverse order of setup, allowing
                // each to complete before stopping the next.
                nfsdInstance.SafeStop();
                nfsDThread.Join();

                mountdInstance.SafeStop();
                mountDThread.Join();

                portMapperInstance.SafeStop();
                portMapperThread.Join();

                return 0;
            }
            catch (System.Net.Sockets.SocketException)
            {
#if DEBUG
                Console.WriteLine("Error: NFS server already running!");
                Console.WriteLine("Close 'nfs' in Task Manager and try again");
#endif
                return 1;
            }
            catch (System.Exception e)
            {
#if DEBUG
                Console.WriteLine("Fatal internal error in NFS server!");
                Console.Write(e.Message);
                Console.WriteLine("Stack trace for debugging:");
                Console.Write(e.StackTrace);
#endif
                return 2;
            }
        }
	}
}
